unsigned long previousTime = 0;
byte seconds ;
byte minutes ;
byte hours ;
int pin1 = 9;
int pin2 = 10;
int pin3 = 11;
int x, y;


void setup()
{
Serial.begin (9600);
 pinMode(pin1, INPUT);
 pinMode(pin2, INPUT);
 pinMode(pin3, OUTPUT);

 digitalWrite(pin3, LOW);

}

void loop ()
{
// I  think using microseconds is even more accurate

x = digitalRead(pin1);
  //if (millis() >= (previousTime))
  if(x == 1)
  {
     previousTime = previousTime + 1000;  // use 1000000 for uS
     seconds = seconds +1;
     if (seconds == 60)
     {
        seconds = 0;
        minutes = minutes +1;
     }
     if (minutes == 60)
     {
        minutes = 0;
        hours = hours +1;
     }
     if (hours == 13)
     {
        hours = 1;
     }
  Serial.print (hours, DEC);
  Serial.print (":");
  Serial.print (minutes,DEC);
  Serial.print (":");
  Serial.println(seconds,DEC);
  } // end 1 second

 

  if(minutes == 1)
  {
    digitalWrite(pin3, HIGH);
  }

  y = digitalRead(pin2);

  if(y == 1)
  {
    digitalWrite(pin3, LOW);
  }

  if(x == 0)
  {
    previousTime = 0;
    seconds = 0;
    minutes = 0;
    hours = 0;
  }

  delay(1000); //This will ensure one loop per second

} // end loop 