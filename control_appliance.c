

// PIN Number for the SIM
#define PINNUMBER ""

 int led1 = 4;
 int led2 = 5;
 int led3 = 6;
 int led4 = 7;
 char inchar;


// initialize the library instances
GSM gsmAccess;
GSM_SMS sms;

// Array to hold the number a SMS is retreived from
char senderNumber[20];

void setup() {
  // initialize serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Serial.println("SMS Messages Receiver");

  // connection state
  boolean notConnected = true;

  // Start GSM connection
  while (notConnected) {
    if (gsmAccess.begin(PINNUMBER) == GSM_READY) {
      notConnected = false;
    } else {
      Serial.println("Not connected");
      delay(1000);
    }
  }

 // prepare the digital output pins
 pinMode(led1, OUTPUT);
 pinMode(led2, OUTPUT);
 pinMode(led3, OUTPUT);
 pinMode(led4, OUTPUT);


 // initially all are off
 digitalWrite(led1, LOW);
 digitalWrite(led2, LOW);
 digitalWrite(led3, LOW);
 digitalWrite(led4, LOW);


  Serial.println("GSM initialized");
  Serial.println("Waiting for messages");
}

void loop() {
  char c;

  // If there are any SMSs available()
  if (sms.available()) {
    Serial.println("Message received from:");

    // Get remote number
    sms.remoteNumber(senderNumber, 20);
    Serial.println(senderNumber);

    // An example of message disposal
    // Any messages starting with # should be discarded
    /*if (sms.peek() == '#') {
      Serial.println("Discarded SMS");
      sms.flush();
    } */

    // Read message bytes and print them
    c = sms.read();
    while (c != '#'){
      c = sms.read();
    }

     if (c=='#')
       {
             //Serial.write(a);
             inchar=sms.read(); //read next char i.e 1
             //Serial.write(inchar);
             if (inchar=='0')
             {
                 digitalWrite(led1, LOW);
             }
             else if (inchar=='1')
             {
                 digitalWrite(led1, HIGH);
             }


       delay(10);
             inchar=sms.read(); //read next char i.e 0
             //Serial.write(inchar);
             if (inchar=='0')
             {
                 digitalWrite(led2, LOW);
             }
             else if (inchar=='1')
             {
                 digitalWrite(led2, HIGH);
             }

       delay(10);
             inchar=sms.read(); //read next char i.e 1
             //Serial.write(inchar);
             if (inchar=='0')
             {
                 digitalWrite(led3, LOW);
             }
             else if (inchar=='1')
             {
                 digitalWrite(led3, HIGH);
             }
  delay(10);
             inchar=sms.read(); //read next char i.e 0
             //Serial.write(inchar);
             if (inchar=='0')
             {
                 digitalWrite(led4, LOW);
             }
             else if (inchar=='1')
             {
                 digitalWrite(led4, HIGH);
             }

       delay(10);
       }

    /*while (c = sms.read()) {
      Serial.print(c);
            
    } */

    Serial.println("\nEND OF MESSAGE");

    // Delete message from modem memory
    sms.flush();
    Serial.println("MESSAGE DELETED");
  }

  delay(1000);

}

